Categories:Writing
License:GPL-3.0
Web Site:https://federicoiosue.github.io/Omni-Notes/
Source Code:https://github.com/federicoiosue/Omni-Notes
Issue Tracker:https://github.com/federicoiosue/Omni-Notes/issues

Auto Name:Omni Notes
Summary:Writes multimedia notes, checklists, reminders
Description:
Note taking open-source application aimed to have both a simple interface but
keeping smart behavior.
.

Repo Type:git
Repo:https://github.com/federicoiosue/Omni-Notes.git

Build:5.2.20,230
    commit=5.2.20
    subdir=omniNotes
    gradle=foss
    prebuild=sed -i -e '/dl.bintray.com/d' -e '/apply plugin: "sonar-runner"/,+999d' ../build.gradle && \
        sed -i -e '/playCompile/d' build.gradle

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:5.2.20
Current Version Code:230
